const fs = require('fs');


async function problem1()
{
    try
    {
        const createDirectoryAsync= await createDirectory();
        const createRandomJsonFilesAsync=await createRandomJsonFiles(3);
        const deleteFileAsync=await deleteFile(3);

    }catch(err)
    {
            throw(err)
    }
}



    function createDirectory()
    {
        return new Promise((resolve,reject)=>{
            fs.mkdir("./dummy", {recursive:true},err=>{
                if(err) 
                {
                    reject("Error occured "+err);
                }
                else resolve("directory successfully created");
            })
        })
    }

    function createRandomJsonFiles(numberOfFilesNeeded)
    {
        return new Promise((resolve,reject)=>{
            for(let index=0;index<numberOfFilesNeeded;index++){
                fs.writeFile("./dummy/JSON"+index+".json", "message inside", (err) => {
                    if (err) 
                    {
                        reject(err);
                    }
                    else resolve("file successfully created");
                })
            }
        })
    }

    function deleteFile(numberOfFilesNeeded)
    {
        return new Promise((resolve,reject)=>{
            for(let index=0; index<numberOfFilesNeeded;index++){
                fs.unlink("./dummy/JSON"+index+".json", (err)=>{
                    if(err) 
                    {
                        reject(err);
                    }
                    else resolve("file successfully deleted");
                })
            }
        })
    }


module.exports = problem1;