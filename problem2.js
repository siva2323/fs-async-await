const fs=require("fs");


async function problem2(filePath)
{
    try
    {
        let lipsumData=await readFunction("lipsum.txt",filePath);
        (await upperCaseFunction(lipsumData,filePath));
        (await addingFileNamesFunction("upperCase.txt",filePath));
        let upperCaseData=await readFunction("upperCase.txt",filePath);
        (await lowerCaseFunction(upperCaseData,filePath));
        (await addingFileNamesFunction("\nlowerCase.txt",filePath));
        let lowerCaseData=await readFunction("lowerCase.txt",filePath);
        (await sortingFunction(lowerCaseData,filePath));
        (await addingFileNamesFunction("\nsorted.txt",filePath));
        let fileListData =await readFunction("fileNames.txt",filePath);
        (await deleteFunction(fileListData,filePath));

    }catch(err)
    {
        throw (err)
    }
}





function readFunction(file,filePath){
    return new Promise((resolve,reject)=>{
        fs.readFile(filePath+file, 'utf-8',(err,data) =>{
            if(err) reject(err);
            else resolve(data);
        })
    })
}

function upperCaseFunction(data,filePath){                                                  
    return new Promise((resolve,reject)=>{
        fs.writeFile(filePath+"upperCase.txt",data.toUpperCase(), err =>{
            if(err) reject(err);
            else resolve("created upperCase.txt ");
        })
    })
}

function addingFileNamesFunction(fileName,filePath){
    return new Promise((resolve,reject)=>{
        fs.appendFile(filePath+"fileNames.txt", fileName, err=>{
            if(err) reject(err);
            else resolve("file name added");
        })
    })
}

function lowerCaseFunction(data,filePath){
    return new Promise((resolve,reject)=>{
        fs.writeFile(filePath+"lowerCase.txt",data.toLowerCase().replaceAll(".",".\n"),err=>{
            if(err) reject(err);
            else resolve("lowerCase.txt written");
        })
    })
}

function sortingFunction(data,filePath){
    return new Promise((resolve,reject)=>{
        fs.writeFile(filePath+"sorted.txt",data.split("\n").sort().join("\n"),err=>{
            if(err) reject(err);
            else resolve("successfully written sorted.txt");
        })
    })
}

function deleteFunction(data,filePath){
    return new Promise((resolve,reject)=>{
        let deleteList = data.split("\n");
        for(let index in deleteList){
            fs.unlink(filePath+deleteList[index],err=>{
                if(err) reject(err);
                else resolve(deleteList+" are deleted");
            })
        }
    })
}


module.exports = problem2;


